// Node.js doesn't have a built-in multipart/form-data parsing library.
// Instead, we can use the 'formidable' library from NPM to parse these requests.
const IncomingForm = require('formidable-serverless').IncomingForm;
const XLSX = require('xlsx');
const twilio = require('twilio');

// const process = {
//   env: {
//     accountSid: 'AC92cba9081b2b55197c9287623ec94ed7',
//     authToken: 'b0a1282c9a8f0806dd311f2b8b9143ba',
//     fromPhoneNumber: '+32 460 25 72 09',
//     notifyServiceSid: 'IS5338f052a97bc42594519919cdad5fbc'
//   },
// };

const { fromPhoneNumber, accountSid, authToken, notifyServiceSid } = process.env;
const client = twilio(accountSid, authToken);

const removeAllNonDigits = (value) => {
  return value.replace(/[^0-9$+]/g, '');
}

const replaceZero = (value) => {
  return value.replace('0', '+32');
}

const createPhoneNumbers = (cells, firstSheet) => {
  const phoneNumbers = [];
  cells.forEach((cell) => {
    const rowNumber = parseInt(cell.replace(/^\D+/g, '')); // replace all leading non-digits with nothing
    const { v: cellValue } = firstSheet[cell];
    console.log(`Processing cell with key: ${cell} & value: ${cellValue} & number ${rowNumber}`);

    if (!cellValue) {
      return;
    }
    let parsedPhoneNumber = removeAllNonDigits(cellValue);
    if (!parsedPhoneNumber) {
      return;
    }
    if (parsedPhoneNumber.charAt(0) === "0") {
      parsedPhoneNumber = replaceZero(parsedPhoneNumber);
    }

    phoneNumbers.push(parsedPhoneNumber);
  });

  return phoneNumbers.map((phoneNumber) => (JSON.stringify({
    binding_type: 'sms',
    address: phoneNumber,
  })));
};

const parseFileToRows = (req) => {
  let form = new IncomingForm();
  return new Promise((resolve, reject) => {
    form.on('file', (field, file) => {
      let workbook = XLSX.readFile(file.path);
      if (!workbook) {
        reject('No workbook found');
      }
      const { Sheets: sheets } = workbook;
      const firstSheet = Object.values(sheets)[0];
      const cells = Object.keys(firstSheet);
      const rows = createPhoneNumbers(cells, firstSheet);

      resolve(rows);
    });
    form.parse(req, (err, fields, files) => {
      console.log('err', err);
      console.log('fields', fields);
      console.log('files', files);
    });

    form.on('end', () => {
      resolve();
    });
  });
};

const sendMessage = (to, message) => {
  if (!to) {
    return null;
  }

  return client.messages
    .create({
      body: message,
      from: fromPhoneNumber,
      to: to
    })
    .then((message) => console.log(message.sid));
};

const callClientWithAudio = (to) => {
  return client.calls
    .create({
      twiml: '<Response><Play>https://storage.googleapis.com/vrt-undercover-bucket/test.mp3</Play></Response>',
      to: to,
      from: fromPhoneNumber,
    })
    .then((call) => console.log(call.sid))
    .catch((e) => console.log(e));
};

const sendBulkMessages = (phoneNumbers, body = 'Heb gehoord dat jij wil helpen? F.B.') => {
  const service = client.notify.services(notifyServiceSid);
  return service.notifications
    .create({
      toBinding: phoneNumbers,
      body: body,
    })
    .then((notification) => {
      console.log(notification);
      return notification;
    })
    .catch((err) => {
      console.error(err);
    });
};

const broadCastEvents = async (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');

  const {audioFile, phoneNumber, message} = req.query || {};
  const messageToSend = message || 'Heb gehoord dat jij wil helpen? F.B.';
  let number = phoneNumber;

  if (number && number.charAt(0) === "0") {
    number = replaceZero(number);
  }
  if (audioFile && number) {
    callClientWithAudio(number);
  } else if (number) {
    sendMessage(number, messageToSend);
  }

  if (req.method !== 'POST') {
    // Return a "method not allowed" error
    return res.status(405).end();
  }

  const phoneNumbers = await parseFileToRows(req);
  if (phoneNumbers && phoneNumbers.length > 0) {
    await sendBulkMessages(phoneNumbers);
  }

  res.send(phoneNumbers);
};

exports.broadCastEvents = broadCastEvents;